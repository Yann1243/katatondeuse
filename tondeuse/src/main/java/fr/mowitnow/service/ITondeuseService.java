package fr.mowitnow.service;

import fr.mowitnow.entities.Coordonnees;

public interface ITondeuseService {
	
	//recuperation des donn�es en entree
	public void extractData(String data);
	//donne la position de la tondeuse
	public String parcours();
	
	public Coordonnees gauche(Coordonnees coord);
	
	public Coordonnees droite(Coordonnees coord);
	
	public Coordonnees avancer(Coordonnees coord);

}
