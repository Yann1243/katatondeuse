package fr.mowitnow.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import fr.mowitnow.entities.Coordonnees;
import fr.mowitnow.entities.Direction;
import fr.mowitnow.main.Main;
import fr.mowitnow.service.ITondeuseService;

public class TondeuseService implements ITondeuseService{
	private static Logger logger = Logger.getLogger(Main.class);
	
	private int[] terrain = new int[] { 5, 5 };
    private List<String> trajets = new ArrayList<String>();

  //recuperation des donn�es en entree
    public void extractData(String data) {
    	String[] result = new String[3]; 
    	result[0] = data.substring(0, 3);
    	result[1] = data.substring(3, 19);
    	result[2] = data.substring(19, 36);
 
        this.terrain[0] = Character.getNumericValue(result[0].charAt(0));
        this.terrain[1] = Character.getNumericValue(result[0].charAt(2));

        for (int i = 1; i < result.length; i++) {
            trajets.add(result[i].replace(" ", ""));
        }
    }

  //donne la position de la tondeuse
    public String parcours() {
        String result = "";
        for (int i = 0; i < this.trajets.size(); i++) {
            Coordonnees init = new Coordonnees();
            init.setX(Character.getNumericValue(trajets.get(i).charAt(0)));
            init.setY(Character.getNumericValue(trajets.get(i).charAt(1)));
            init.setDirection(Direction.valueOf(String.valueOf(trajets.get(i).charAt(2))));

            String instructions = trajets.get(i).substring(3, trajets.get(i).length());
            for (int j = 0; j < instructions.length(); j++) {
                if (instructions.charAt(j) == 'D') {
                    init = droite(init);
                }

                if (instructions.charAt(j) == 'G') {
                    init = gauche(init);
                }

                if (instructions.charAt(j) == 'A') {
                    init = avancer(init);
                }

            }

            result += init.toString() + " ";

        }

        return result;
    }

    public Coordonnees gauche(Coordonnees coord) {
        if (coord.getDirection().equals(Direction.E)) {
            coord.setDirection(Direction.S);
        } else if (coord.getDirection().equals(Direction.S)) {
            coord.setDirection(Direction.W);
        } else if (coord.getDirection().equals(Direction.W)) {
            coord.setDirection(Direction.N);
        } else if (coord.getDirection().equals(Direction.N)) {
            coord.setDirection(Direction.E);
        }

        return coord;
    }

    public Coordonnees droite(Coordonnees coord) {
        if (coord.getDirection().equals(Direction.E)) {
            coord.setDirection(Direction.N);
        } else if (coord.getDirection().equals(Direction.S)) {
            coord.setDirection(Direction.E);
        } else if (coord.getDirection().equals(Direction.W)) {
            coord.setDirection(Direction.S);
        } else if (coord.getDirection().equals(Direction.N)) {
            coord.setDirection(Direction.W);
        }

        return coord;
    }

    public Coordonnees avancer(Coordonnees coord) {
        if ((coord.getX() == this.terrain[0] && coord.getDirection().equals(Direction.W)) ||
                (coord.getY() == this.terrain[1] && coord.getDirection().equals(Direction.N)) ||
                (coord.getY() == 0 && coord.getDirection().equals(Direction.S)) ||
                (coord.getX() == 0 && coord.getDirection().equals(Direction.E))) {
            return coord;
        }

        if (coord.getDirection().equals(Direction.E)) {
            coord.setX(coord.getX() - 1);
        }

        if (coord.getDirection().equals(Direction.W)) {
            coord.setX(coord.getX() + 1);
        }

        if (coord.getDirection().equals(Direction.S)) {
            coord.setY(coord.getY() - 1);
        }

        if (coord.getDirection().equals(Direction.N)) {
            coord.setY(coord.getY() + 1);
        }

        return coord;

    }

}
