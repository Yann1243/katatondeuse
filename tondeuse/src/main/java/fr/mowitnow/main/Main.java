package fr.mowitnow.main;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

import org.apache.log4j.Logger;

import fr.mowitnow.service.ITondeuseService;
import fr.mowitnow.service.impl.TondeuseService;

public class Main {

	private static String fileWork;

	private static Logger logger = Logger.getLogger(Main.class);

	public static void main(String[] args) {
		 
		String formatDateComplet	= "dd/MM/yyyy HH':'mm";
		
		SimpleDateFormat dateFormat = new SimpleDateFormat(formatDateComplet);
		logger.info("################################################################################");
		logger.info("### Debut du traitement (" + dateFormat.format(new Date()) + ") ###");		
		
		try {
			fileWork = "..\\tondeuse\\src\\test\\resources\\test.txt";
			final File file = new File(fileWork);
			
			if(file.exists()) {
				logger.info("Fichier re�u :" + file.getName());
				
				Scanner scan = new Scanner(new File(fileWork));
		        
		        while (scan.hasNextLine()) {
		           
		        	 String data = scan.nextLine();
		            
					
					ITondeuseService tondeuseServ = new TondeuseService() ;
					
					logger.info("donn�e dans le fichier :" + data);
					tondeuseServ.extractData(data);
					
					logger.info("position finale de La tondeuse :" + tondeuseServ.parcours());
		         
		        }
		        scan.close();
		        
		       
			}else {
				logger.info("Aucun fichier en entr�e");
			}
		

		}catch(Exception e){
			logger.error("### Erreur lors du traitement (" + dateFormat.format(new Date()) + ") ###");
			e.printStackTrace();
		}finally{
			logger.info("### Fin du traitement (" + dateFormat.format(new Date()) + ") ###");			
			logger.info("################################################################################");
		}

	}

}
